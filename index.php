<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/dateTimePicker.css">
</head>
<body>
<div class="kontejner">
    <div>

        <div id="logo">
            <img src="images/DALMATIKA_logo-1.png"/>
        </div>

        <div class="red">
            <div class="kol-xss-12">
                <div id="calendar" data-toggle="calendar"></div>
            </div>
        </div>

        <div>
            <a href="novipokusaj.html" target="_self">
                <img src="images/big blue arrow.png" height="42" width="42" title="Idi na najam broda"/>
                <p>Najam brodova</p>
            </a>
        </div>

        <div id="footer" class="pocisti left">
            <div id="footer_logo" class="left pocisti">
                <h1><p>DALMATIKA D.O.O.</p></h1>
            </div>
            <div id="footer_middle" class="left pocisti">
                <p>Domjanićeva 9</p>
                <p>10 000 Zagreb</p>
                <p>+385 1 4664 535</p>
                <p><a href="mailto:info@dalmatika.hr" target="_blank">info@dalmatika.hr</a></p>
            </div>
            <div id="footer_right" class="left pocisti">
                <ul>
                    <li><p><a href="http://dalmatika.hr/?page_id=38" target="_blank">O nama</a></p></li>
                    <li><p><a href="http://dalmatika.hr/?page_id=42" target="_blank">Lokacija</a></p></li>
                    <li><p><a href="http://dalmatika.hr/?page_id=40" target="_blank">Kontakt</a></p></li>
                </ul>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="scripts/components/jquery.min.js"></script>
    <script type="text/javascript" src="scripts/dateTimePicker.min.js"></script>
    <script type="text/javascript">

        var onCalndarDateSelect = function(date, month, year){
            alert([year, month, date].join('-') + ' is: ' + this.isAvailable(date, month, year));
        };

        $(document).ready(function () {
            $('#calendar').calendar({
                day_first: 6,
                month_name: ['Siječanj', 'Veljača', 'Ožujak', 'Travanj', 'Svibanj', 'Lipanj', 'Srpanj', 'Kolovoz', 'Rujan', 'Listopad', 'Studeni', 'Prosinac'],
                day_name: ['Pon', 'Uto', 'Sri', 'Čet', 'Pet', 'Sub', 'Ned'],
                onSelectDate: onCalndarDateSelect,
                num_prev_month: 1,
                num_next_month: 1,
                adapter: '/server/dohvati.php'
            });
        });
    </script>
</body>
</html>