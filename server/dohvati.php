<?php

include 'baza.php';

$godina = isset($_GET['godina']) ? $_GET['godina'] : date('Y');//'2018';
$mjesec = isset($_GET['mjesec']) ? $_GET['mjesec'] : date('m');//'06';

if(strlen($mjesec) == 1){
	$mjesec = '0' . $mjesec;
}

$sql = "SELECT DAY(Start) AS Start, DAY(Stop) AS Stop FROM `kalendar` WHERE DATE_FORMAT(Start, '%Y-%m') IN('$godina-$mjesec')";

$result=mysqli_query($link,$sql);
if($result === false){
	die("ERROR: Could not able to execute $sql. " . mysqli_error($link));
}

// Associative array
$res = array();
/*$broj_dana = cal_days_in_month(CAL_GREGORIAN, $mjesec, $godina);
for($i = 1; $i <= $broj_dana; $i++){
		$res[$i] = 1;
}*/

while($row=mysqli_fetch_assoc($result)){
	/*$start = $row['Start'];
	$stop = $row['Stop'];
	for($j = $start; $j <= $stop; $j++){
		$res[$j] = 0;
	}*/
	$res[] = $row;
}
echo(json_encode($res));

// Free result set
mysqli_free_result($result);
 
// close connection
mysqli_close($link);