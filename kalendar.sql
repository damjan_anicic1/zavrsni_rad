-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 15, 2018 at 04:19 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kalendar`
--

-- --------------------------------------------------------

--
-- Table structure for table `kalendar`
--

CREATE TABLE `kalendar` (
  `id` int(10) UNSIGNED NOT NULL,
  `Prezime` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Start` date NOT NULL,
  `Stop` date NOT NULL,
  `BrojPutnika` smallint(6) NOT NULL,
  `Telefon` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kalendar`
--

INSERT INTO `kalendar` (`id`, `Prezime`, `Start`, `Stop`, `BrojPutnika`, `Telefon`, `Email`) VALUES
(1, 'Test', '2018-06-06', '2018-06-20', 12, '098566', 'damjan.anicic1@gmail.com'),
(2, 'dasda', '2018-06-12', '2018-06-26', 2, '5252', 'aaa'),
(3, 'Klisovi&#263;', '2018-08-04', '2018-08-11', 7, '0993567854', 'miona.klisovic@gmail.com'),
(4, 'kiki', '2018-07-20', '2018-07-28', 4, '098319020', 'dalmatik@gmail.com'),
(5, 'kutlesa', '2018-12-05', '2018-12-15', 34, '1023190381', 'adasda@yahoo.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kalendar`
--
ALTER TABLE `kalendar`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kalendar`
--
ALTER TABLE `kalendar`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
