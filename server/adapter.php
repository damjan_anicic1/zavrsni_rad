<?php
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Content-Language, Authorization');
header('Access-Control-Expose-Headers: Authorization');

# CONFIG
define('_DB_HOST', 'localhost');
define('_DB_NAME', 'kalendar');
define('_DB_USER', 'root');
define('_DB_PASS', '');

# DB CONNECT
$connection = mysqli_connect(_DB_HOST, _DB_USER, _DB_PASS, _DB_NAME) or
die ('Unable to connect to MySQL server.');

# ACTION
$keys = isset($_REQUEST['keys']) ? $_REQUEST['keys'] : array();
if (!is_array($keys)) {
    $keys = array($keys);
}
$keys = array_filter($keys);

# RESULT
$unavailable = array();
if (!empty($keys)) {
    $sql = "SELECT * FROM calendar WHERE DATE_FORMAT(date, '%Y-%m') IN ('" . implode("', '", $keys) . "')";
    $sql_result = mysqli_query($sql, $connection) or die ('request "Could not execute SQL query" ' . $sql);
    while ($red = mysqli_fetch_assoc($sql_result)) {
        $unavailable[] = $red['date'];
    }
}
echo(json_encode($unavailable));
exit();
